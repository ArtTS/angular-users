import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {IUser} from "../models/user.interface";
import {HttpClient} from "@angular/common/http";
import {UsersService} from "./users.service";

@Injectable({
  providedIn: 'root'
})
export class UsersApiService {
  private URL_API: string = 'https://jsonplaceholder.typicode.com/users';
  private localStorageKey: string = 'usersLocal';

  public entities$: BehaviorSubject<IUser[]> = new BehaviorSubject<IUser[]>([]);
  public entitiesUserDetail$: BehaviorSubject<IUser | null> = new BehaviorSubject<IUser | null>(null);

  constructor(private http: HttpClient, private usersService: UsersService) {}

  getUsers() {
    const storedUser = localStorage.getItem(this.localStorageKey);

    if (storedUser) {
      this.usersService.users = JSON.parse(storedUser);
      this.entities$.next(this.usersService.users);
    } else {
      this.http.get<IUser[]>(this.URL_API).subscribe({
        next: (data: IUser[]) => {
          this.usersService.users = data;
          this.entities$.next(this.usersService.users);
          localStorage.setItem(this.localStorageKey, JSON.stringify(this.usersService.users));
          console.log('data', data);
        },
        error: err => console.log(err)
      })
    }
  }

  getUser(id: number) {
    const storedUser = localStorage.getItem(this.localStorageKey);
    const url: string = `${this.URL_API}/${id}`;

    if (storedUser) {
      this.usersService.userDetail = (JSON.parse(storedUser)).find((el: IUser) => el.id === id);

      console.log(this.usersService.userDetail);

      this.entitiesUserDetail$.next(this.usersService.userDetail);
    } else {
      this.http.get<IUser>(url).subscribe({
        next: (data: IUser) => {
          this.usersService.userDetail = data;
          this.entitiesUserDetail$.next(this.usersService.userDetail);

          console.log('data', data);
        },
        error: err => console.log(err)
      })
    }
  }

  deleteUser(id: number) {
    const url: string = `${this.URL_API}/${id}`

    this.http.delete(url).subscribe({
      next: (data) => {
        this.usersService.users = this.usersService.users.filter(el => el.id !== id);
        this.entities$.next(this.usersService.users);

        localStorage.setItem(this.localStorageKey, JSON.stringify(this.usersService.users));
      }
    })
  }

  editUser(data: {id: number, name: string, username: string, email: string}) {
    let updatingUser: IUser = this.usersService.users.find((el) => el.id === data.id)!;
    let newUser: IUser = {
      ...updatingUser,
      name: data.name,
      username: data.username,
      email: data.email,
    }

    const url: string = `${this.URL_API}/${data.id}`;

    this.http.patch<IUser>(url, newUser).subscribe({
      next: (data: IUser) => {
        this.usersService.users = this.usersService.users.map(el => el.id === data.id ? data : el);
        this.entities$.next(this.usersService.users);
        localStorage.setItem(this.localStorageKey, JSON.stringify(this.usersService.users));
      },
      error: (err) => console.log('error', err)
    })
  }

  addUser(data: {id: number, name: string, username: string, email: string}) {
    const newUser: IUser = {
      id: data.id,
      name: data.name,
      username: data.username,
      email: data.email,
      address: {
        "street": "street",
        "suite": "suite",
        "city": "city",
        "zipcode": "zipcode",
        "geo": {
          lat: 'lat',
          lng: 'lng',
        }
      },
      phone: '+78505465189',
      website: 'google.com',
      company: {
        name: 'company name',
        catchPhrase: 'company catchPhrase',
        bs: 'company bs',
      }
    }

    console.log('newUser', newUser);

    this.http.post<IUser>(this.URL_API, newUser).subscribe({
      next: (newData: IUser) => {
        const newEl: IUser = {...newData, id: Number(new Date()),} // Здесь почему-то ID всегда 11, поэтому насильно еще раз переписываю ID
        console.log('add', newEl);

        this.usersService.users.push(newEl);
        this.entities$.next(this.usersService.users);
        localStorage.setItem(this.localStorageKey, JSON.stringify(this.usersService.users));
      },
      error: (err) => console.log('error', err)
    });
  }
}
